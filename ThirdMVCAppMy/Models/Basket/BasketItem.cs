﻿
namespace ThirdMVCAppMy.Models.Basket
{
    public class BasketItem
    {
        public int DishId { get; set; }
        public int RestaurantId { get; set; }
        public int Qty { get; set; }
    }
}
