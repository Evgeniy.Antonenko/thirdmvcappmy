﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using ThirdMVCAppMy.Models.Dish;
using ThirdMVCAppMy.Models.Restaurant;

namespace ThirdMVCAppMy.Models.Basket
{
    public class BasketModel
    {
        [Display(Name = "Пользователь")]
        public string UserName { get; set; }

        public List<DishModel> Dishes { get; set; }

        public List<RestaurantModel> Restaurants { get; set; }

        [Display(Name = "Общая сумма выбранных блюд")]
        public decimal TotalAmount { get; set; }

        [Display(Name = "Кол-во выбранных блюд")]
        public int TotalQty { get; set; }

        public int RestaurantId { get; set; }
    }
}
