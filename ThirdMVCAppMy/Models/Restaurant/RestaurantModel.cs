﻿using System.ComponentModel.DataAnnotations;

namespace ThirdMVCAppMy.Models.Restaurant
{
    public class RestaurantModel
    {
        public int Id { get; set; }


        [Display(Name = "Наименование")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Описание")]
        [Required]
        public string Description { get; set; }


    }
}
