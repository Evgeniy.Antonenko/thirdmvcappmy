﻿
namespace ThirdMVCAppMy.Models.Dish
{
    public enum DishSortState
    {
        NameAsc,
        NameDesc,
        DescriptionAsc,
        DescriptionDesc,
        PriceAsc,
        PriceDesc
    }
}
