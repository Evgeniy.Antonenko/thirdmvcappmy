﻿using System.ComponentModel.DataAnnotations;
using ThirdMVCAppMy.Models.Restaurant;

namespace ThirdMVCAppMy.Models.Dish
{
    public class DishCreateModel
    {
        public int Id { get; set; }

        [Display(Name = "Наименование")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Описание")]
        [Required]
        public string Description { get; set; }

        [Display(Name = "Ресторан/Кафе")]
        [Required]
        public string RestaurantName { get; set; }
        
        [Display(Name = "Цена")]
        [Required]
        public decimal Price { get; set; }

        public RestaurantModel RestaurantModel { get; set; }

        public int RestaurantId { get; set; }
    }
}
