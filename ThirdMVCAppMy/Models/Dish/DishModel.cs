﻿using System.ComponentModel.DataAnnotations;

namespace ThirdMVCAppMy.Models.Dish
{
    public class DishModel
    {
        public int Id { get; set; }

        [Display(Name = "Наименование")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "Описание")]
        [Required]
        public string Description { get; set; }

        [Display(Name = "Ресторан/Кафе")]
        [Required]
        public string RestaurantName { get; set; }

        [Display(Name = "Цена")]
        [Required]
        public decimal Price { get; set; }
        
        public int RestaurantId { get; set; }

        [Display(Name = "Количество")]
        [Required]
        public int Qty { get; set; }

        [Display(Name = "Общая сумма блюд")]
        [Required]
        public decimal TotalAmountDish { get; set; }
    }
}
