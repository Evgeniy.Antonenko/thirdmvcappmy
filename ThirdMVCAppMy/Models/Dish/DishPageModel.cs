﻿using System;
using System.Collections;

namespace ThirdMVCAppMy.Models.Dish
{
    public class DishPageModel : IEnumerable
    {
        public int PageNumber { get; set; }
        public int TotalPages { get; set; }

        public DishPageModel(int count, int pageNumber, int pageSize)
        {
            PageNumber = pageNumber;
            TotalPages = (int)Math.Ceiling(count / (double)pageSize);
        }

        public bool HasPreviousPage => PageNumber > 1;

        public bool HasNextPage => PageNumber < TotalPages;
        public IEnumerator GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}
