﻿using System.Collections.Generic;
using ThirdMVCAppMy.Models.Restaurant;

namespace ThirdMVCAppMy.Models.Dish
{
    public class DishSortAndPageModel
    {
        public RestaurantModel RestaurantModel { get; set; }

        public DishModel DishModel { get; set; }

        public List<DishModel> Dishes { get; set; }

        public DishSortState NameSort { get; set; }

        public DishSortState DescriptionSort { get; set; }

        public DishSortState PriceSort { get; set; }

        public DishSortState DishSortState { get; set; }

        public int? Page { get; set; }

        public DishPageModel DishPageModel { get; set; }
    }
}
