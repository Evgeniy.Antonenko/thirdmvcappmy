﻿using System.ComponentModel.DataAnnotations;

namespace ThirdMVCAppMy.Models.OrderBasket
{
    public class OrderBasketDetailsModel
    {
        public int DishId { get; set; }

        [Display(Name = "Название блюда")]
        public string Name { get; set; }

        [Display(Name = "Описание")]
        public string Description { get; set; }

        [Display(Name = "Ресторан")]
        public string RestaurantName { get; set; }

        [Display(Name = "Цена")]
        public decimal Price { get; set; }

        [Display(Name = "Кол-во")]
        public int Qty { get; set; }

        [Display(Name = "Сумма")]
        public decimal TotalAmountDish { get; set; }
    }
}
