﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ThirdMVCAppMy.Models.OrderBasket
{
    public class OrderBasketModel
    {
        public int Id { get; set; }

        [Display(Name = "Пользователь")]
        public string UserName { get; set; }

        public string DishBasket { get; set; }

        [Display(Name = "Дата заказа")]
        public DateTime CreatedOrder { get; set; }

        [Display(Name = "Общая сумма заказа")]
        public decimal TotalAmount { get; set; }
    }
}
