﻿using System.Threading.Tasks;
using ThirdMVCAppMy.DAL.Entities;
using ThirdMVCAppMy.Models.Basket;

namespace ThirdMVCAppMy.Services.Baskets.Contracts
{
    public interface IBasketService
    {
        Task AddToBasketAsync(int dishId, User user);
        BasketModel GetBasketList(User currentUser, int restaurantId);
        void AddOrderBasket(BasketModel model, User user);
        Task DeleteFromBasketAsync(int dishId, int dishQty, User currentUser);
    }
}
