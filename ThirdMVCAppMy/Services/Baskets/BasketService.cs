﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using ThirdMVCAppMy.DAL;
using ThirdMVCAppMy.DAL.Entities;
using ThirdMVCAppMy.Models.Basket;
using ThirdMVCAppMy.Models.Dish;
using ThirdMVCAppMy.Services.Baskets.Contracts;
using ThirdMVCAppMy.Services.Dishes.Contracts;

namespace ThirdMVCAppMy.Services.Baskets
{
    public class BasketService : IBasketService
    {
        private readonly UserManager<User> _userManager;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IDishService _dishService;

        public BasketService(UserManager<User> userManager, IUnitOfWorkFactory unitOfWorkFactory, IDishService dishService)
        {
            if (userManager == null)
                throw new ArgumentNullException(nameof(userManager));
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));
            if (dishService == null)
                throw new ArgumentNullException(nameof(dishService));

            _userManager = userManager;
            _unitOfWorkFactory = unitOfWorkFactory;
            _dishService = dishService;
        }


        public async Task AddToBasketAsync(int dishId, User user)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var dish = unitOfWork.Dishes.GetFullDishById(dishId);
                if (dish == null)
                    throw new ArgumentOutOfRangeException(nameof(dishId), $"No dish with id {dishId}");

                int restaurantId = dish.RestaurantId;

                var basketItems = string.IsNullOrWhiteSpace(user.Basket) ?
                    CreateBasket(dishId, restaurantId) :
                    UpdateBasket(dishId, restaurantId, user.Basket);

                user.Basket = JsonConvert.SerializeObject(basketItems);
                await _userManager.UpdateAsync(user);
            }
        }
        
        public BasketModel GetBasketList(User currentUser, int restaurantId)
        {
            BasketModel basketModel = new BasketModel();
            basketModel.Dishes = new List<DishModel>();
            basketModel.UserName = currentUser.UserName;
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var dishes = _dishService.GetDishModelsWithRestaurant();
                string currentBasket = currentUser.Basket;
                if (string.IsNullOrEmpty(currentBasket))
                {
                    return basketModel;
                }
                List<BasketItem> allItems = JsonConvert.DeserializeObject<List<BasketItem>>(currentBasket);
                List<BasketItem> currentItems = allItems.Where(c => c.RestaurantId == restaurantId).ToList();
                foreach (var basket in currentItems)
                {
                    DishModel dish = dishes.FirstOrDefault(p => p.Id == basket.DishId);
                    dish.Qty = basket.Qty;
                    dish.TotalAmountDish = dish.Price * dish.Qty;
                    basketModel.Dishes.Add(dish);
                    basketModel.TotalAmount += dish.TotalAmountDish;
                    basketModel.TotalQty += dish.Qty;
                    basketModel.RestaurantId = restaurantId;
                }
                return basketModel;
            }
        }

        public void AddOrderBasket(BasketModel model, User user)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var order = new OrderBasket();
                order.UserId = user.Id;
                order.TotalAmount = model.TotalAmount;
                order.CreatedOrder = DateTime.Now;
                order.DishBasket = JsonConvert.SerializeObject(model.Dishes);
                unitOfWork.OrderBaskets.Create(order);
            }
        }

        public async Task DeleteFromBasketAsync(int dishId, int dishQty, User currentUser)
        {
            List<BasketItem> basketItems = new List<BasketItem>();
            basketItems = dishQty > 1  ?
                DeleteDishFomBasket(dishId, currentUser) :
                DeleteAllDishFomBasket(dishId, currentUser);
            currentUser.Basket = JsonConvert.SerializeObject(basketItems);
            await _userManager.UpdateAsync(currentUser);
        }

        private List<BasketItem> UpdateBasket(int dishId, int restaurantId, string currentBasket)
        {
            try
            {
                var currentItems = JsonConvert.DeserializeObject<List<BasketItem>>(currentBasket);
                var existingItem = currentItems.FirstOrDefault(c => c.DishId == dishId);
                if (existingItem != null)
                {
                    existingItem.Qty++;
                }
                else
                {
                    currentItems.Add(
                        new BasketItem()
                        {
                            DishId = dishId,
                            RestaurantId = restaurantId,
                            Qty = 1
                        }
                    );
                }

                return currentItems;
            }
            catch
            {
                return CreateBasket(dishId, restaurantId);
            }
        }

        private List<BasketItem> CreateBasket(int dishId, int restaurantId)
        {
            return new List<BasketItem>()
            {
                new BasketItem()
                {
                    DishId = dishId,
                    RestaurantId = restaurantId,
                    Qty = 1
                }
            };
        }

        private List<BasketItem> DeleteAllDishFomBasket(int dishId, User currentUser)
        {
            List<BasketItem> currentItems = new List<BasketItem>();
            currentItems = JsonConvert.DeserializeObject<List<BasketItem>>(currentUser.Basket);
            BasketItem item = currentItems.FirstOrDefault(i => i.DishId == dishId);
            currentItems.Remove(item);
            return currentItems;
        }
        private List<BasketItem> DeleteDishFomBasket(int dishId, User currentUser)
        {
            List<BasketItem> currentItems = new List<BasketItem>();
            currentItems = JsonConvert.DeserializeObject<List<BasketItem>>(currentUser.Basket);
            BasketItem item = currentItems.FirstOrDefault(i => i.DishId == dishId);
            item.Qty = item.Qty - 1;
            
            return currentItems;
        }
    }
}
