﻿
using System.Collections.Generic;
using ThirdMVCAppMy.Models.Restaurant;

namespace ThirdMVCAppMy.Services.Restaurans.Contracts
{
    public interface IRestaurantService
    {
        List<RestaurantModel> GetDishesModelsList();
        void CreateRestaurant(RestaurantCreateModel model);
    }
}
