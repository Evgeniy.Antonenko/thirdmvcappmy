﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ThirdMVCAppMy.DAL;
using ThirdMVCAppMy.DAL.Entities;
using ThirdMVCAppMy.Models.Restaurant;
using ThirdMVCAppMy.Services.Restaurans.Contracts;

namespace ThirdMVCAppMy.Services.Restaurans
{
    public class RestaurantService : IRestaurantService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public RestaurantService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));

            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public List<RestaurantModel> GetDishesModelsList()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var restaurants = unitOfWork.Restaurants.GetAll().ToList();
                return Mapper.Map<List<RestaurantModel>>(restaurants);
            }
        }

        public void CreateRestaurant(RestaurantCreateModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var brand = Mapper.Map<Restaurant>(model);
                unitOfWork.Restaurants.Create(brand);
            }
        }
    }
}
