﻿using System.Collections.Generic;
using ThirdMVCAppMy.DAL.Entities;
using ThirdMVCAppMy.Models.OrderBasket;

namespace ThirdMVCAppMy.Services.OrderBaskets.Contracts
{
    public interface IOrderBasketService
    {
        List<OrderBasketModel> GetOrderBasketList(User currentUser);
        List<OrderBasketDetailsModel> GetDishOrderBasket(int id);
    }
}
