﻿using System;
using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using ThirdMVCAppMy.DAL;
using ThirdMVCAppMy.DAL.Entities;
using ThirdMVCAppMy.Models.Dish;
using ThirdMVCAppMy.Models.OrderBasket;
using ThirdMVCAppMy.Services.OrderBaskets.Contracts;

namespace ThirdMVCAppMy.Services.OrderBaskets
{
    public class OrderBasketService : IOrderBasketService
    {
        private readonly UserManager<User> _userManager;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public OrderBasketService(UserManager<User> userManager, IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (userManager == null)
                throw new ArgumentNullException(nameof(userManager));
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));

            _userManager = userManager;
            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public List<OrderBasketModel> GetOrderBasketList(User currentUser)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                int userId = currentUser.Id;
                var orderBuckets = unitOfWork.OrderBaskets.GetOrderBasketByUserId(userId);
                List<OrderBasketModel> orderFromBucketModels = Mapper.Map<List<OrderBasketModel>>(orderBuckets);

                foreach (var item in orderFromBucketModels)
                {
                    item.UserName = currentUser.UserName;
                }

                return orderFromBucketModels;
            }
        }

        public List<OrderBasketDetailsModel> GetDishOrderBasket(int id)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                List<DishModel> dishesOrderBasket = new List<DishModel>();
                var dishesOrder = unitOfWork.OrderBaskets.GetById(id);
                dishesOrderBasket = JsonConvert.DeserializeObject<List<DishModel>>(dishesOrder.DishBasket);
                var dishOrderBasketModels = Mapper.Map<List<OrderBasketDetailsModel>>(dishesOrderBasket);

                return dishOrderBasketModels;
            }
        }
    }
}
