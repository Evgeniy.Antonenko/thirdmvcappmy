﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ThirdMVCAppMy.DAL;
using ThirdMVCAppMy.DAL.Entities;
using ThirdMVCAppMy.Models.Dish;
using ThirdMVCAppMy.Models.Restaurant;
using ThirdMVCAppMy.Services.Dishes.Contracts;

namespace ThirdMVCAppMy.Services.Dishes
{
    public class DishService : IDishService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;

        public DishService(IUnitOfWorkFactory unitOfWorkFactory)
        {
            if (unitOfWorkFactory == null)
                throw new ArgumentNullException(nameof(unitOfWorkFactory));

            _unitOfWorkFactory = unitOfWorkFactory;
        }

        public DishSortAndPageModel GetDihSortPageModel(int id, DishSortAndPageModel model, DishSortState sortState)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var restaurant = unitOfWork.Restaurants.GetById(id);
                RestaurantModel restaurantModel = Mapper.Map<RestaurantModel>(restaurant);

                var dishes = unitOfWork.Dishes.GetFullDishesByIdRestaurant(id).ToList();
                List<DishModel> dishModels = Mapper.Map<List<DishModel>>(dishes);

                var dishSortAndPageModel = dishModels.Sort(sortState);

                int pageSize = 3;
                int count = dishes.Count();
                int page = model.Page.HasValue ? model.Page.Value : 1;
                DishPageModel dishPageModel = new DishPageModel(count, page, pageSize);
                dishSortAndPageModel.Dishes = dishSortAndPageModel.Dishes.Skip((page - 1) * pageSize).Take(pageSize).ToList();

                dishSortAndPageModel.DishSortState = sortState;
                dishSortAndPageModel.DishPageModel = dishPageModel;
                dishSortAndPageModel.Page = page;
                dishSortAndPageModel.RestaurantModel = restaurantModel;
                dishSortAndPageModel.RestaurantModel.Id = id;

                return dishSortAndPageModel;
            }
        }

        public List<DishModel> GetDishesModelsList(int id)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var dishes = unitOfWork.Dishes.GetFullDishesByIdRestaurant(id).ToList();
                List<DishModel> dishModels= Mapper.Map<List<DishModel>>(dishes);
                return Mapper.Map<List<DishModel>>(dishModels);
            }
        }

        public DishCreateModel GetDishCreateModel(int restaurantId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var restaurant = unitOfWork.Restaurants.GetById(restaurantId);

                var dishCreateModel = new DishCreateModel()
                {
                    RestaurantId = restaurantId,

                    RestaurantModel = Mapper.Map<RestaurantModel>(restaurant)
                };

                return dishCreateModel;
            }
        }

        public void CreateDish(DishCreateModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var dish = Mapper.Map<Dish>(model);
                unitOfWork.Dishes.Create(dish);
            }
        }

        public List<DishModel> GetDishModelsWithRestaurant()
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var products = unitOfWork.Dishes.GetFullDishesWithRestaurant();
                return Mapper.Map<List<DishModel>>(products);
            }
        }
    }
}
