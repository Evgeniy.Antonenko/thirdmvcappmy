﻿using System.Collections.Generic;
using ThirdMVCAppMy.Models.Dish;

namespace ThirdMVCAppMy.Services.Dishes.Contracts
{
    public interface IDishService
    {
        DishSortAndPageModel GetDihSortPageModel(int id, DishSortAndPageModel model, DishSortState sortState);
        List<DishModel> GetDishesModelsList(int id);
        DishCreateModel GetDishCreateModel(int restaurantId);
        void CreateDish(DishCreateModel model);
        List<DishModel> GetDishModelsWithRestaurant();
    }
}
