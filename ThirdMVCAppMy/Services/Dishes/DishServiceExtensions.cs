﻿using System.Collections.Generic;
using System.Linq;
using ThirdMVCAppMy.Models.Dish;

namespace ThirdMVCAppMy.Services.Dishes
{
    public static class DishServiceExtensions
    {
        public static DishSortAndPageModel Sort(this List<DishModel> dishModels, DishSortState sortState)
        {
            var dishSortAndPageModel = new DishSortAndPageModel()
            {
                NameSort = sortState == DishSortState.NameAsc ? DishSortState.NameDesc : DishSortState.NameAsc,
                DescriptionSort = sortState == DishSortState.DescriptionAsc ? DishSortState.DescriptionDesc : DishSortState.DescriptionAsc,
                PriceSort = sortState == DishSortState.PriceAsc ? DishSortState.PriceDesc : DishSortState.PriceAsc
            };

            switch (sortState)
            {
                case DishSortState.NameAsc:
                   dishModels = dishModels.OrderBy(d => d.Name).ToList();
                    break;
                case DishSortState.NameDesc:
                    dishModels = dishModels.OrderByDescending(d => d.Name).ToList();
                    break;
                case DishSortState.DescriptionAsc:
                    dishModels = dishModels.OrderBy(d => d.Description).ToList();
                    break;
                case DishSortState.DescriptionDesc:
                    dishModels = dishModels.OrderByDescending(d => d.Description).ToList();
                    break;
                case DishSortState.PriceAsc:
                    dishModels = dishModels.OrderBy(d => d.Price).ToList();
                    break;
                case DishSortState.PriceDesc:
                    dishModels = dishModels.OrderByDescending(d => d.Price).ToList();
                    break;
            }

            dishSortAndPageModel.Dishes = dishModels;
            return dishSortAndPageModel;
        }
    }
}
