﻿using AutoMapper;
using ThirdMVCAppMy.DAL.Entities;
using ThirdMVCAppMy.Models.Dish;
using ThirdMVCAppMy.Models.OrderBasket;
using ThirdMVCAppMy.Models.Restaurant;

namespace ThirdMVCAppMy
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateDishToDishModelMap();
            CreateModelMap<Restaurant, RestaurantModel>();
            CreateModelMap<Restaurant, RestaurantCreateModel>();
            CreateModelMap<Dish, DishCreateModel>();
            CreateModelMap<OrderBasket, OrderBasketModel>();
            CreateModelMap<DishModel, OrderBasketDetailsModel>();
        }

        private void CreateModelMap<From, To>()
        {
            CreateMap<From, To>();
            CreateMap<To, From>();
        }

        private void CreateDishToDishModelMap()
        {
            CreateMap<Dish, DishModel>()
                .ForMember(target => target.RestaurantName,
                    src => src.MapFrom(p => p.Restaurant.Name));
        }
    }
}
