﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ThirdMVCAppMy.Models.Dish;
using ThirdMVCAppMy.Services.Dishes.Contracts;

namespace ThirdMVCAppMy.Controllers
{
    public class DishController : Controller
    {
        private readonly IDishService _dishService;

        public DishController(IDishService dishService)
        {
            if (dishService == null)
                throw new ArgumentNullException(nameof(dishService));

            _dishService = dishService;
        }

        [Authorize]
        [HttpGet]
        public IActionResult Index(int? id, DishSortAndPageModel model, DishSortState sortState = DishSortState.NameAsc)
        {
            if (!id.HasValue)
                throw new ArgumentOutOfRangeException(nameof(id));

            var dishes = _dishService.GetDihSortPageModel(id.Value, model, sortState);
            return View(dishes);
        }

        [Authorize]
        [HttpGet]
        public IActionResult CreateDish(int? restaurantId)
        {
            if (!restaurantId.HasValue)
                throw new ArgumentOutOfRangeException(nameof(restaurantId));

            var model = _dishService.GetDishCreateModel(restaurantId.Value);

            return View(model);
        }

        [Authorize]
        [HttpPost]
        public IActionResult CreateDish(DishCreateModel model)
        {
            try
            {
                int id = model.RestaurantId;
                _dishService.CreateDish(model);

                return RedirectToAction("Index", "Dish", new{id});
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
