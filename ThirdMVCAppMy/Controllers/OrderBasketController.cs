﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using ThirdMVCAppMy.DAL.Entities;
using ThirdMVCAppMy.Services.OrderBaskets.Contracts;

namespace ThirdMVCAppMy.Controllers
{
    public class OrderBasketController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly IOrderBasketService _orderBasketService;

        public OrderBasketController(UserManager<User> userManager, IOrderBasketService orderBasketService)
        {
            if (userManager == null)
                throw new ArgumentNullException(nameof(userManager));
            if (orderBasketService == null)
                throw new ArgumentNullException(nameof(orderBasketService));
            _userManager = userManager;
            _orderBasketService = orderBasketService;
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            User currentUser = await _userManager.GetUserAsync(User);
            var orderBasketModels = _orderBasketService.GetOrderBasketList(currentUser);
            return View(orderBasketModels);
        }

        [Authorize]
        [HttpGet]
        public IActionResult Details(int id)
        {
            var orderBucketDetails = _orderBasketService.GetDishOrderBasket(id);
            return View(orderBucketDetails);
        }
    }
}
