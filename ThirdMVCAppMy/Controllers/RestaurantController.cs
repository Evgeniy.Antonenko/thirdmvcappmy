﻿using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ThirdMVCAppMy.Models.Restaurant;
using ThirdMVCAppMy.Services.Restaurans.Contracts;

namespace ThirdMVCAppMy.Controllers
{
    public class RestaurantController : Controller
    {
        private readonly IRestaurantService _restaurantService;

        public RestaurantController(IRestaurantService restaurantService)
        {
            if (restaurantService == null)
                throw new ArgumentNullException(nameof(restaurantService));

            _restaurantService = restaurantService;
        }


        [HttpGet]
        public IActionResult Index()
        {
            var model = _restaurantService.GetDishesModelsList();
            return View(model);
        }

        [Authorize]
        [HttpGet]
        public IActionResult CreateRestaurant()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public IActionResult CreateRestaurant(RestaurantCreateModel model)
        {
            try
            {
                _restaurantService.CreateRestaurant(model);

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}
