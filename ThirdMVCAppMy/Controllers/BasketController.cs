﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ThirdMVCAppMy.DAL.Entities;
using ThirdMVCAppMy.Models.Basket;
using ThirdMVCAppMy.Services.Baskets.Contracts;

namespace ThirdMVCAppMy.Controllers
{
    public class BasketController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly IBasketService _basketService;

        public BasketController(UserManager<User> userManager, IBasketService basketService)
        {
            if (basketService == null)
                throw new ArgumentNullException(nameof(basketService));
            if(userManager == null)
                throw new ArgumentNullException(nameof(userManager));

            _userManager = userManager;
            _basketService = basketService;
        }


        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Index(int? restaurantId)
        {
            if (!restaurantId.HasValue)
            {
                ViewBag.BadRequestMessage = "Restaurat Id can not be NULL";
                return View("BadRequest");
            }

            User currentUser = await _userManager.GetUserAsync(User);
            var deferredProductModels = _basketService.GetBasketList(currentUser, restaurantId.Value);
            return View(deferredProductModels);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> AddToOrder(int restaurantId)
        {
            User currentUser = await _userManager.GetUserAsync(User);
            var basketItem = _basketService.GetBasketList(currentUser, restaurantId);
            _basketService.AddOrderBasket(basketItem, currentUser);
            List<BasketItem> currentItems = JsonConvert.DeserializeObject<List<BasketItem>>(currentUser.Basket);
            var stayInbasketItems = JsonConvert.SerializeObject(currentItems.Where(b => b.RestaurantId != restaurantId).ToList());
            currentUser.Basket = stayInbasketItems;
            await _userManager.UpdateAsync(currentUser);
            return RedirectToAction("Index", "Restaurant");
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Add(int? dishId, int? restaurantId)
        {
            if (!restaurantId.HasValue)
            {
                ViewBag.BadRequestMessage = "Restaurat Id can not be NULL";
                return View("BadRequest");
            }

            int id = restaurantId.Value;

            if (!dishId.HasValue)
            {
                ViewBag.BadRequestMessage = "Dish Id can not be NULL";
                return View("BadRequest");
            }

            try
            {
                User currentUser = await _userManager.GetUserAsync(User);
                await _basketService.AddToBasketAsync(dishId.Value, currentUser);
                return RedirectToAction("Index", "Dish", new{id});
            }
            catch (Exception e)
            {
                return StatusCode(500, e);
            }
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> DeleteFomBasket(int dishId, int dishQty, int restaurantId)
        {
            User currentUser = await _userManager.GetUserAsync(User);
            await _basketService.DeleteFromBasketAsync(dishId, dishQty, currentUser);
            //return dishQty <= 1 
            //    ? RedirectToAction("Index", "Dish", new { id = restaurantId }) 
            //    : RedirectToAction("Index", "Basket", new { restaurantId });
            return RedirectToAction("Index", "Basket", new {restaurantId});
        }
    }
}
