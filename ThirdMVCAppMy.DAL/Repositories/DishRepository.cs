﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ThirdMVCAppMy.DAL.Entities;
using ThirdMVCAppMy.DAL.Repositories.Contracts;

namespace ThirdMVCAppMy.DAL.Repositories
{
    public class DishRepository : Repository<Dish>, IDishRepository
    {
        public DishRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Dishes;
        }

        public IEnumerable<Dish> GetFullDishesWithRestaurant()
        {
            return entities.Include(d => d.Restaurant);
        }

        public IEnumerable<Dish> GetFullDishesByIdRestaurant(int id)
        {
            return GetFullDishesWithRestaurant().Where(p => p.RestaurantId == id);
        }

        public Dish GetByIdFullDishes(int id)
        {
            return GetFullDishesWithRestaurant().FirstOrDefault(d => d.Id == id);
        }

        public Dish GetFullDishById(int id)
        {
            return GetFullDishesWithRestaurant().FirstOrDefault(p => p.Id == id);
        }
    }
}
