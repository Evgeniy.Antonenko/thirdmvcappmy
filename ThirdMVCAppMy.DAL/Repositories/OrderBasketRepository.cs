﻿using System.Collections.Generic;
using System.Linq;
using ThirdMVCAppMy.DAL.Entities;
using ThirdMVCAppMy.DAL.Repositories.Contracts;

namespace ThirdMVCAppMy.DAL.Repositories
{
    public class OrderBasketRepository : Repository<OrderBasket>, IOrderBasketRepository
    {
        public OrderBasketRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.OrderBaskets;
        }

        public IEnumerable<OrderBasket> GetOrderBasketByUserId(int id)
        {
            return entities.Where(e => e.UserId == id);
        }
    }
}
