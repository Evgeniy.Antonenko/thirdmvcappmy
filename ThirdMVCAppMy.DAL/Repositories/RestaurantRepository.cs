﻿using ThirdMVCAppMy.DAL.Entities;
using ThirdMVCAppMy.DAL.Repositories.Contracts;

namespace ThirdMVCAppMy.DAL.Repositories
{
    public class RestaurantRepository : Repository<Restaurant>, IRestaurantRepository
    {
        public RestaurantRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Restaurants;
        }
    }
}
