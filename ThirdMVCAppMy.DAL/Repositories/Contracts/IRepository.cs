﻿using System.Collections.Generic;
using ThirdMVCAppMy.DAL.Entities.Contracts;

namespace ThirdMVCAppMy.DAL.Repositories.Contracts
{
    public interface IRepository<T> where T : class, IEntity
    {
        T Create(T entity);

        T GetById(int id);

        IEnumerable<T> GetAll();

        T Update(T entity);

        void Remove(T entity);
    }
}
