﻿using System.Collections.Generic;
using ThirdMVCAppMy.DAL.Entities;

namespace ThirdMVCAppMy.DAL.Repositories.Contracts
{
    public interface IOrderBasketRepository : IRepository<OrderBasket>
    {
        IEnumerable<OrderBasket> GetOrderBasketByUserId(int id);
    }
}
