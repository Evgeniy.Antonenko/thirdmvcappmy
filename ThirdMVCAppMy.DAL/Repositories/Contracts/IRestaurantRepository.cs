﻿using ThirdMVCAppMy.DAL.Entities;

namespace ThirdMVCAppMy.DAL.Repositories.Contracts
{
    public interface IRestaurantRepository : IRepository<Restaurant>
    {
    }
}
