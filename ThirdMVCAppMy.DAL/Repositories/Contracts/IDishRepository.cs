﻿using System.Collections.Generic;
using ThirdMVCAppMy.DAL.Entities;

namespace ThirdMVCAppMy.DAL.Repositories.Contracts
{
    public interface IDishRepository : IRepository<Dish>
    {
        IEnumerable<Dish> GetFullDishesByIdRestaurant(int id);
        IEnumerable<Dish> GetFullDishesWithRestaurant();
        Dish GetByIdFullDishes (int id);
        Dish GetFullDishById(int id);
    }
}
