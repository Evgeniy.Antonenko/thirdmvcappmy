﻿using ThirdMVCAppMy.DAL.Entities;
using ThirdMVCAppMy.DAL.EntitiesConfiguration.Contracts;

namespace ThirdMVCAppMy.DAL.EntitiesConfiguration
{
    public class EntityConfigurationsContainer : IEntityConfigurationsContainer
    {
        public IEntityConfiguration<Restaurant> RestaurantConfiguration { get; }
        public IEntityConfiguration<Dish> DishConfiguration { get; }
        public IEntityConfiguration<OrderBasket> OrderBasketConfiguration { get; }

        public EntityConfigurationsContainer()
        {

            RestaurantConfiguration = new RestaurantConfiguration();
            DishConfiguration = new DishConfiguration();
            OrderBasketConfiguration = new BaseEntityConfiguration<OrderBasket>();
        }
    }
}
