﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ThirdMVCAppMy.DAL.Entities;

namespace ThirdMVCAppMy.DAL.EntitiesConfiguration
{
    public class RestaurantConfiguration : BaseEntityConfiguration<Restaurant>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Restaurant> builder)
        {
            builder
                .Property(r => r.Name)
                .HasMaxLength(450)
                .IsRequired();
            builder
                .Property(r => r.Description)
                .HasMaxLength(int.MaxValue)
                .IsRequired();
            builder
                .HasIndex(r => r.Name)
                .IsUnique();
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<Restaurant> builder)
        {
            builder
                .HasMany(r => r.Dishes)
                .WithOne(r => r.Restaurant)
                .HasForeignKey(c => c.RestaurantId)
                .IsRequired();
        }
    }
}
