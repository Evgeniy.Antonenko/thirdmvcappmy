﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ThirdMVCAppMy.DAL.Entities;

namespace ThirdMVCAppMy.DAL.EntitiesConfiguration
{
    public class OrderBasketConfiguration : BaseEntityConfiguration<OrderBasket>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<OrderBasket> builder)
        {
            builder.Property(o => o.DishBasket)
                .HasMaxLength(int.MaxValue);
            builder.Property(o => o.TotalAmount)
                .HasDefaultValue(0.0M)
                .IsRequired();
            builder.HasIndex(o => o.Id).IsUnique(false);
        }
    }
}
