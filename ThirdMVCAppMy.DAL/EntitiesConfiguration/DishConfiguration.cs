﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ThirdMVCAppMy.DAL.Entities;

namespace ThirdMVCAppMy.DAL.EntitiesConfiguration
{
    public class DishConfiguration : BaseEntityConfiguration<Dish>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Dish> builder)
        {
            builder
                .Property(d => d.Name)
                .HasMaxLength(450)
                .IsRequired();
            builder
                .Property(d => d.Description)
                .HasMaxLength(int.MaxValue)
                .IsRequired();
            builder.Property(p => p.Price)
                .HasDefaultValue(0.0M)
                .IsRequired();
            builder
                .HasIndex(d => d.Name)
                .IsUnique();
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<Dish> builder)
        {
            builder
                .HasOne(d => d.Restaurant)
                .WithMany(p => p.Dishes)
                .HasForeignKey(p => p.RestaurantId)
                .IsRequired();
        }
    }
}
