﻿using ThirdMVCAppMy.DAL.Entities;

namespace ThirdMVCAppMy.DAL.EntitiesConfiguration.Contracts
{
    public interface IEntityConfigurationsContainer
    {
        IEntityConfiguration<Restaurant> RestaurantConfiguration { get; }
        IEntityConfiguration<Dish> DishConfiguration { get; }
        IEntityConfiguration<OrderBasket> OrderBasketConfiguration { get; }
    }
}
