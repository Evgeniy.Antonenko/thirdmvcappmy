﻿using System;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ThirdMVCAppMy.DAL.Entities.Contracts;

namespace ThirdMVCAppMy.DAL.EntitiesConfiguration.Contracts
{
    public interface IEntityConfiguration<T> where T : class, IEntity
    {
        Action<EntityTypeBuilder<T>> ProvideConfigurationAction();
    }
}
