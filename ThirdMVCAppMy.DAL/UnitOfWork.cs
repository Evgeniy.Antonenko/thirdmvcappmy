﻿using System;
using ThirdMVCAppMy.DAL.Repositories;
using ThirdMVCAppMy.DAL.Repositories.Contracts;

namespace ThirdMVCAppMy.DAL
{
    public class UnitOfWork : IDisposable
    {
        private readonly ApplicationDbContext _context;
        private bool _disposed;

        public IRestaurantRepository Restaurants { get; set; }
        public IDishRepository Dishes { get; set; }
        public IOrderBasketRepository OrderBaskets { get; set; }
        
        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;

            Restaurants = new RestaurantRepository(context);
            Dishes = new DishRepository(context);
            OrderBaskets = new OrderBasketRepository(context);
        }

        #region Disposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
                _context.Dispose();

            _disposed = true;
        }

        ~UnitOfWork()
        {
            Dispose(false);
        }
        #endregion
    }
}
