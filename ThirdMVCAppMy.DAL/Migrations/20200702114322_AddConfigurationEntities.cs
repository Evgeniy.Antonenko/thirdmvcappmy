﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ThirdMVCAppMy.DAL.Migrations
{
    public partial class AddConfigurationEntities : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Basket",
                table: "AspNetUsers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Basket",
                table: "AspNetUsers");
        }
    }
}
