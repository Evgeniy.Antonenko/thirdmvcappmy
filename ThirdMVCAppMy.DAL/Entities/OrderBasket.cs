﻿using System;
using System.Collections.Generic;
using System.Text;
using ThirdMVCAppMy.DAL.Entities.Contracts;

namespace ThirdMVCAppMy.DAL.Entities
{
    public class OrderBasket : IEntity
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string DishBasket { get; set; }
        public decimal TotalAmount { get; set; }
        public DateTime CreatedOrder { get; set; }
    }
}
