﻿using Microsoft.AspNetCore.Identity;
using ThirdMVCAppMy.DAL.Entities.Contracts;

namespace ThirdMVCAppMy.DAL.Entities
{
    public class Role : IdentityRole<int>, IEntity
    {
    }
}
