﻿using ThirdMVCAppMy.DAL.Entities.Contracts;

namespace ThirdMVCAppMy.DAL.Entities
{
    public class Dish : IEntity
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int RestaurantId { get; set; }
        public Restaurant Restaurant { get; set; }
    }
}
