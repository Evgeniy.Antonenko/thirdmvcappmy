﻿using Microsoft.AspNetCore.Identity;
using ThirdMVCAppMy.DAL.Entities.Contracts;

namespace ThirdMVCAppMy.DAL.Entities
{
    public class User : IdentityUser<int>, IEntity
    {
        public string Basket { get; set; }
    }
}
