﻿
namespace ThirdMVCAppMy.DAL
{
    public interface IUnitOfWorkFactory
    {
        UnitOfWork Create();
    }
}
